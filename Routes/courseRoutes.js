/*
	To be able to create routes from anotehr file to be sued in our application, from here, we have to import express as well, however, we will now use anotehr method from express to contain our routes.

	the Router() method will allow us to contain our routes

*/


const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

const { verify, verifyAdmin } = auth;

// all routes to courses now has an endpoint prefaced with /courses

// endpoint - /courses/
// Gets all course documents whether it is active or inactie
router.get('/', verify, verifyAdmin, courseControllers.getAllCourses);

// verifyAdmin() will disallow regular logged in and non-logged in users from using this route
// only logged in user is able to to use addCourse
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// Get all active courses for regular and non-logged in users
router.get('/activeCourses', courseControllers.getActiveCourses);


// get single course
//router.post('/getSingleCourse', courseControllers.getSingleCourse);

// we can pass data in a route without the use of request body by passing a small amount of data through the url with the use of route params
// /endpoint/:routeParam
// http://localhost:4000/courses/getSingleCourse/62e8c60b1b5871ba9be4d05d
router.get('/getSingleCourse/:courseId', courseControllers.getSingleCourse);


// update a single course
// pass the id of the course we want to updagte via route params
// the update detaisl will be passed via request body
router.put("/updateCourse/:courseId", verify, verifyAdmin, courseControllers.updateCourse);


// archive a single course
// pass the id for teh course we want to update via route params
// we will directly update the course as inactive

router.delete("/archiveCourse/:courseId", verify, verifyAdmin, courseControllers.archiveCourse);





module.exports = router;