const express = require("express");
const router = express.Router();


// routes should just contain the endpoints and igt should only trigger the function but it should be where we define the logic of our function

// import our user controllers
const userControllers = require("../controllers/UserControllers");

// check the imported userControllers:
//console.log(userControllers);

// import auth to be able to have access and use the verify methids to act as middleware for the routes
// Middleware add in the route such as verify() will have access to the req, res objects
const auth = require("../auth");
// destructure auth to get only our methods and save it in variables:
const { verify } = auth;




/*
	Updated route syntax:
	method("endpoint", handler function)
*/


// register
router.post("/", userControllers.registerUser);

// verify() is used as a middleware which means our request will get through verify first before our controller
// verify() will not only check the avlidity of the token but also add the decoded data of the token in the request object as req.user

router.get('/details', verify, userControllers.getUserDetails);


// Route for user authentication
router.post('/login', userControllers.loginUser);



// check email
router.post('/checkEmail', userControllers.checkEmail);



// user enrollment
// courseId will come from the req.body
// userID will come from the req.user

router.post ('/enroll', verify, userControllers.enroll);




module.exports = router;