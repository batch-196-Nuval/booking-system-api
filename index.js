const express = require("express");
const mongoose = require("mongoose");
// mongoose is an ODM library
const app = express();
const port = 4000

/*
	Mongoose Connection

	mongoose.connect() is a method to connect to our API with our mongoDB database via the use of mongoose. IOt ahs 2 arguments. 1st is the connection string to connect to our API to our mongoDB. 2nd. is an object sued to add information between mongoose and mongoDB

	replace / change password in the connection string to your DB user password

	just before the ? in the connection string, add the database name
*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.blg7xae.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewURLParser: true,
	useUnifiedTopology: true
});

// we will create notifications if the connection to the db is a success or failed
let db = mongoose.connection;
// this is to show notification of an internal server error from MongoDB
db.on('error', console.error.bind(console, "MongoDB Connection Error."));

// if the connection is open and successful, we will output a message in the terminal
db.once('open', () => console.log("Connected to MongoDB."));


app.use(express.json());


// import our routes and use it as a middleware
// which means, that we will be able to group together our routes

const courseRoutes = require('./routes/courseRoutes');
// use our routes and group them together under '/courses'
// our endpoingts are now prefaced with /courses
app.use('/courses', courseRoutes);



// import our user routes
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);





app.listen(port,()=>console.log(`Server is running at port ${port}`));