/*
	The naming convention for model files is the singular and capitalized form of the name of the document
*/


const mongoose = require ("mongoose");


/*
	Mongoose Schema

	Before we can create documents from our API to save into our database, we must first determine the structure of the documents to be written in the database

	schema acts as a blueprint for aour data / document

	A schema is a representation of how a document is structured. It also determines the tyoes of data and the expected properties

	so, with this, we won't have to worry for it if we had input "stock" or "stocks" in our documents, because we can make it uniform with a schema

	In mongoose, to create a schema, we use the Schema() constructor from mongoose. This will allow us to create a new mongoose schema object
*/

const courseSchema = new mongoose.Schema({

	name: {
		type:String,
		required: [true, "Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."] 
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required."]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]

});

// module.exports - so we can import and use this file in another file
/*
	Mongoose Model

	is the connection to our collection

	It has 2 arguments, 1st the name of the collection the model is going to connect to. In MongoDB. once we create a new course document this model will connect to our course document this model will connect to our courses collection. But since the courses collection is not yet created initially, mongoDB will create it for you.

	the 2nd argument is the schema of the documents in the collection
*/
module.exports = mongoose.model ("Course", courseSchema);