/*
	Naming convention for Controllers is that it should be named after the model / documents it is concerned with.
*/

// the business logic of our API should be in Controllers

// Controllers are functions which contain the actual business logic of our API and is triggered by a route

// MVC - Models, Views, Controllers



// import the User Model in the controllers instead because this is where we are now going to use it
const User = require("../models/User");

// import course model
const Course = require("../models/Course");


// import bcrypt
const bcrypt = require("bcrypt"); // bcrypt is a package which allows us to hash our passwords to add a layer of security for our user's details

// import auth.js module to sue createAccessToken and its subsequent methods
const auth = require("../auth");


// To create a controller, we first add it into our module.exports so that we can import our controllers from our module
// userControllers.registerUser
module.exports.registerUser = (req, res) => {

	//console.log(req.body) // check the input passed via the client

	// using bcrypt, we are going to hide the user's password underneath a layer of randomized charcters. Salt rounds are the number of times we randomized the string / password hash

	const hashedPw = bcrypt.hashSync(req.body.password, 10);
	console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};
// getUserDetails should only allow the Logged-in user to get his own details
module.exports.getUserDetails = (req, res) => {

	console.log(req.user); // ==> contains the details of the logged in user
	//User.find({"_id": req.body.id}) // find will return an array which matches the criteria. It will return an array even if it only found 1 document. findOne() will return a single document that matched our criteria
	// OR
	//User.findOne({_id: req.body.id})	OR
	// findById() is a mongoose method that will allow us to find a document stricly by its id
	// this will allow us to ensure that the LOGGED IN USER or the USER THAT PASSED THE TOKEN will be able to get HIS OWN details and ONLY his own (User.findBy(req.user.id))

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.loginUser = (req, res) => {

	console.log(req.body);
	/*
		steps for logging in our user:
		1. find the user by its email
		2. if we found the user, we will check the password if the password input and hashed password in our db matches
		3. if we don't find a user, we will send a message to the client
		4. if upon checking the found user's passsword is the same our input password, we will generate a "key" for the user to have authorization to access certain features in our app

	*/
	// mongoDB: db.users.findOne({email: req.body.email})
	User.findOne({"email": req.body.email})
	.then(foundUser => {
		// foundUser is the parameter that contains the result of findOne
		// findOne() returns null if it is not able to match any document
		if (foundUser === null){
			return res.send({message: "No User found."})
			// client wil receive this object with our message if no user is found
		} else {
			//console.log(foundUser)
			// if we find a user, foundUser will contain the document that meatched the input email
			// check if the input password from req.body matches the hashed password in our foundUser document
			/*
				bcrypt.compareSync(<inputString>, <hashedString>)

				"spidermanOG"
				"$2a$10$szz7dUCIYSrs/FOQqvjMwOMePc0j.z041qtgwQCGqxinyrVxIBmp2"

				if the inputString and the hashedString matches and are the same, the compareSync method will return true, else, it will return false
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			//console.log(isPasswordCorrect);

			// if the password is correct, we will crate a "key", a token for our user, else, we will send a message
			if(isPasswordCorrect){

				/*
					to be able to create a "key" or token that allows / authorizes our logged in user around our application, we have to create our own module called auth.js

					this module will create an encoded string which contains our user's details.

					this encoded string is what we call a JSONWebToken or JWT

					thi JWT can only be properly unwrapped or decoded within our own secret word / string


				*/



				//console.log("We will create a token for the user if the password is correct");


				// auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to the client
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			} else {
				return res.send({message: "Incorrect Password."});
			}

		}
	})

};


// checks if email exists or not
module.exports.checkEmail = (req, res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		// findOne will return null if no match is found
		// send false if email does not exists
		// send true if email exists
		if (result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}

	})
	.catch(error => res.send(error))

}

/*module.exports.checkEmail = (req, res) => {

	User.findOne({email: req.body.email})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}	*/






module.exports.enroll = async (req, res) => {

	// check the id of the user will enroll?
	//console.log(req.user.id);


	// check the id of the course we want to enroll?
	//console.log(req.body.courseId);


	// validate the suer if they are an admin or not
	// if the suer is an admin, send a message to client and end the resposne
	// else, we will continue


	// enrollment will come in 2 steps:
	// first, find the user who is enrolling and update his enrollements subdocument array. we will push the courseId in the enrollments array.

	// second, find the course where we are enrolling and update its enrollees subdocument array. we will push the userId in the enrollees array

	// since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting JavaScript continue per line.

	// async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of the function

	// to be able to wait for the result of a function, we use the await keyword. the await keyword allows us to wait for the function to finish and get a result before processing

	if (req.user.isAdmin) {
		return res.send ({message: "Action Forbidden!"});
	} 

	// return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseID into our user's enrollment subdocument array.


	let isUserUpdated = await User.findById(req.user.id).then (user => {

		// check if you found the user's document
		//console.log(user);

		// add the courseId in an object and push that object into the user's enrollments
		// because we have to follow the schema of the emrollments subdocument array
		let newEnrollment = {

			courseId: req.body.courseId
			//courseId: {} - error checking lang ito

		}

		// access the enrollment array from our user and push the new enrollment subdocument into the enrollements array
		user.enrollments.push(newEnrollment)


		// we must save the user document and return the value of saving our document
		// return true if we oush the subdocument successfully
		// catch and return error message if otehrwise

		return user.save().then(user => true).catch(err => err.message)


	})

	// if user was able to enroll properly, 'isUserUpdated' contains true
	// else, isUserUpdated will contain an error message
	//console.log(isUserUpdated);



	// add an if statement and stop the process if isUserUpdated does not contain true
	if (isUserUpdated !== true) {
		return res.send ({message: isUserUpdated});
	}

	// find the course where we will enroll or add the user as an enrollee adn return true if we were able to push the user into the enrollees array properly or send the error message instead

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		//console.log(course); // contain the found course or the course you want to enroll in

		// create an object to pushed into the subdocument array enrollees
		// we ahve to follow the schema of our subdocument array
		let enrollee = {
			userId: req.user.id
		}

		// push the enrollee into the enrollees subdocuemnt array of the course
		course.enrollees.push(enrollee);

		// save the course document
		// return true if we were able to save and add the suer as enrollee properly
		// return an err meesage if we catch an error
		return course.save().then(course => true).catch(err => err.message)



	})


	//console.log(isCourseUpdated);


	// if isCourseUpdated does not contain true, send the error message to the client and stop the process
	if (isCourseUpdated !== true) {
		return res.send({message: isCourseUpdated})
	}

	// ensure thatw e were able to both update the user and coourse doocument to add our enrollment and enrollee respectively and send a message to the client to end the enrollment procss:
	if (isUserUpdated && isCourseUpdated){

		return res.send({message: "Thank you for enrolling!"})
		
	}





}