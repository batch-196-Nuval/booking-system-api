// Import the course model so we can manipulate it and add a new course document
const Course = require("../models/Course");

module.exports.getAllCourses = (req, res) => {

	//res.send("This route will get all course documents");
	// use the Course model to connect to our collection and retrieve our courses
	// to be able to query into our collections, we use the model connected to that collection
	// in mongoDB: db.courses.find({})
	// Model.find)({}) - return a collection of documents that matches our criteria simiar to mongoDB's .find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))


}

module.exports.addCourse = (req, res) => {

	//console.log(req.body);
	// res.send("This route will create a new course document");


	// using the cpurse model, we will use its constructor to create our Course Document which awill follow then schema of the model and add methods for document creation
	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	// console.log(newCourse);

	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save() is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	// db.courses.insertOne()

	// .then() allows us to process the result of a previous function / method in its own anonymous function

	// .catch() - catches the error and allows us to catch it and send to the client

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getSingleCourse = (req, res) => {

	console.log(req.params);
	// req.params is an obj that contains the value captured via route params
	// the field name of the req.params indicate the name of the route params
	// how do we get the id passed as the route param?
	console.log(req.params.courseId);
	
	Course.findById(req.params.courseId)
	//Course.findOne({_id: req.params.courseId})
	.then(result => res.send(result))
	.catch(error => res.send(error))



}


/*module.exports.getSingleCourse = (req, res) => {

	Course.findOne({_id: req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}	*/




module.exports.updateCourse = (req, res) => {

	// how do we check if we get the id?
	console.log(req.params.courseId);

	// how do we check the update that is the input?
	console.log(req.body);

	// findByIdAndUpdate - used to update documents and has 3 arguments
	// findByIdAndUpdate(<id>, {updates}. {new: true}) - new: true it to update the document

	// We can create a new object to filter update details
	// the indicated fields in the update obj will be the fields updated
	// fields / property that are not part of the update obj will not be updated
	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	Course.findByIdAndUpdate(req.params.courseId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))

}


module.exports.archiveCourse = (req, res) => {

	//console.log(req.params.courseId);

	//console.log(req.body);



	let update = {

		isActive: false

	}

	Course.findByIdAndUpdate(req.params.courseId, update, {new: true})
	.then (result => res.send (result))
	.catch (error => res.send(error))


}